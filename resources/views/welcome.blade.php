<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>RSS</title>

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">

        <script src="/js/app.js"></script>

    </head>
    <body>
        <div class="container">
            @php
                $feeds = \App\Feed::orderBy('date', 'desc')->paginate(30);

                if ($category > -1) {
                    $feeds = \App\Feed::ofCategory($category)->orderBy('date', 'desc')->paginate(30);
                }

                $categories = \App\Category::all();
            @endphp
            <div>
                <form action="/" method="get">
                    <select name="category" onchange="this.form.submit()">
                        <option value="-1" @if ($category == -1) selected @endif>All news</option>
                        @foreach($categories as $cat)
                        <option value="{{$cat->id}}" @if ($category == $cat->id) selected @endif>{{$cat->name}}</option>
                        @endforeach
                    </select>
                </form>
            </div>
            <ul>
            @forelse($feeds as $feed)
                <li>
                    <a href="{{$feed->provider}}" target="_blank">{{$feed->provider}}</a> -
                    <b><span style="cursor: pointer" data-title="{{$feed->title}}" data-description="{{$feed->description}}"
                          data-url="{{$feed->url}}" data-toggle="modal" data-target="#feedModal">{{$feed->title}}</span></b> -
                    {{date(DATE_RFC2822, $feed->date)}}
                </li>
            @empty
                <li><p>No news, feed me!</p></li>
            @endforelse
            <br>
            @if (!is_null($feeds->previousPageUrl()))
                    <a style="color: white;" href="{{$feeds->previousPageUrl() . '&category=' . $category}}">
                        <button type="button" class="btn btn-primary">Previous</button>
                    </a>
            @endif
            @if (!is_null($feeds->nextPageUrl()))
                    <a style="color: white;" href="{{$feeds->nextPageUrl() . '&category=' . $category}}">
                        <button type="button" class="btn btn-primary">Next</button>
                    </a>
            @endif
            </ul>
        </div>
        <div class="modal fade" id="feedModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modal-title-text">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <p id="modal-body-text">One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a id="modal-url" href="" target="_blank"><button type="button" class="btn btn-primary" >Open</button></a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>
            $(function() {
                $('#feedModal').on("show.bs.modal", function (e) {
                    $("#modal-title-text").text($(e.relatedTarget).data('title'));
                    $("#modal-body-text").text($(e.relatedTarget).data('description'));
                    $("#modal-url").attr('href', ($(e.relatedTarget).data('url')));
                });
            });
        </script>
    </body>
</html>
