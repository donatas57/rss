<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin</title>

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">

    </head>
    <body>
    <div class="container">
        @if (Auth::check())
            <div class="container" style="max-width: 460px;">
                <form action="/admin/changePassword" method="post">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Password</span>
                        <input type="password" name="password1" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Repeat password</span>
                        <input type="password" name="password2" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
                    </div>
                    <div class="btn-group" style="margin-left: 354px;" role="group" aria-label="...">
                        <button type="submit" class="btn btn-default">Confirm</button>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                </form>
                <br>
                @php
                    $urls = \App\Url::all();
                    $categories = \App\Category::all();
                @endphp
                <form action="/admin/categories" method="post">
                    <input type="text" name="name"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <button type="submit" class="btn btn-primary">Add Category</button>
                </form>
                <br>
                <form action="/admin/urls" method="post">
                    <input type="text" name="url"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <select name="categoryId">
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-primary">Add URL</button>
                </form>
                <br>
                @forelse($urls as $url)
                    <form action="/admin/urls/{{ $url->id }}" method="post" style="display: inline-block">
                        <input type="text" name="url" value="{{ $url->url }}" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="_method" value="put" />

                        <select name="categoryId">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}" @if ($url->categoryId == $category->id) selected @endif>{{$category->name}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-warning">Edit</button>
                    </form>
                    <form action="/admin/urls/{{ $url->id }}" method="post" style="display: inline-block">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="_method" value="delete" />
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                @empty
                    <p>No urls found.. :(</p>
                @endforelse
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        @else
            <div class="content">
                <form action="/admin/login" method="post">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Admin</span>
                        <input type="text" name="name" class="form-control" placeholder="Admin" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Password</span>
                        <input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
                    </div>
                    <br>
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="submit" class="btn btn-default">Confirm</button>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                </form>
            </div>
        @endif

    </div>
    </body>
</html>
