<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Feed extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'provider', 'description', 'url', 'date', 'categoryId',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'categoryId');
    }

    /**
     * @param $query
     * @param $categoryId
     * @return mixed
     */
    public function scopeOfCategory($query, $categoryId)
    {
        return $query->where('categoryId', $categoryId);
    }
}