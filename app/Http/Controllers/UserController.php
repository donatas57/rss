<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'login']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request)
    {
        $data = $request->all();
        $name = $data['name'];
        $password = $data['password'];

        if (Auth::attempt(['name' => $name, 'password' => $password])) {
            // Authentication passed...
            return redirect('admin');
        }

        return response()->json(['error' => 'Not authorized.'],403);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        $data = $request->all();
        $password1 = $data['password1'];
        $password2 = $data['password2'];

        if ($password1 !== $password2) {
            return response()->json(['error' => 'Password not matched!'],401);
        }

        if (empty($password1)) {
            return response()->json(['error' => 'Password cant be empty!'],401);
        }

        $user = Auth::user();
        $user->password = Hash::make($password1);
        $user->save();

        return response()->json(['result' => 'Password successfully changed!'],200);
    }
}
