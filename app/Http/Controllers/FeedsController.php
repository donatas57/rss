<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeedsController extends Controller
{
    public function showFeeds(Request $request)
    {
        $category = -1;

        if (isset($request->category)) {
            $category = $request->category;
        }

        return view('welcome')->with('category', $category);
    }
}
