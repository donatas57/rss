<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUrl;
use App\Http\Requests\UpdateUrl;
use App\Url;

class UrlController extends Controller
{
    /**
     * UrlController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUrl  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUrl $request)
    {
        Url::create($request->all());

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUrl  $request
     * @param  \App\Url  $url
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUrl $request, Url $url)
    {
        $url->fill($request->all())->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Url  $url
     * @return \Illuminate\Http\Response
     */
    public function destroy(Url $url)
    {
        $url->delete();

        return redirect()->back();
    }
}
