<?php

namespace App\Services;

use App\Feed;
use App\Url;

class FeedService
{
    public function update()
    {
        $urls = Url::all();

        foreach ($urls as $url) {
            $content = $this->getContentFromUrl($url->url);

            if ($content) {
                $this->processContent($content, $url->categoryId);
            }
        }
    }

    /**
     * @param $url
     * @return null|string
     */
    private function getContentFromUrl($url)
    {
        try {
            $content = file_get_contents($url);
        } catch (\Exception $e) {
            echo 'Bad URL found! ';
            return null;
        }

        return $content === false ? null : $content;
    }

    /**
     * @param $content
     * @param $categoryId
     */
    private function processContent($content, $categoryId)
    {
        $xml = simplexml_load_string($content);

        if ($xml !== false) {
            $provider = $xml->channel->link;

            foreach ($xml->channel->item as $item) {
                $this->createFeed([
                    'title' => $item->title,
                    'provider' => $provider,
                    'description' => $item->description,
                    'url' => $item->link,
                    'date' => strtotime($item->pubDate),
                    'categoryId' => $categoryId
                ]);
            }
        }
    }

    /**
     * @param $data
     */
    private function createFeed($data)
    {
        $alreadyUrl = Feed::where('url', $data['url'])->first();

        if (is_null($alreadyUrl)) {
            Feed::create($data);
        }
    }
}