<?php

namespace App\Console\Commands;

use App\Services\FeedService;
use Illuminate\Console\Command;

class FeedUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var FeedService
     */
    protected $feedService;

    /**
     * Create a new command instance.
     *
     * @param FeedService $feedService
     */
    public function __construct(FeedService $feedService)
    {
        $this->feedService = $feedService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->feedService->update();
        $this->info('Feeds successfully updated!');
    }
}
