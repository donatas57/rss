<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Store a newly created
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Welcome!');

        $newUser = $this->ask('Enter your name');

        $alreadyUser = User::where('name', $newUser)->first();

        if (!is_null($alreadyUser)) {
            $this->error('Name already exist!');
            return;
        }

        $password1 = $this->secret('Enter your password');
        $password2 = $this->secret('Repeat your password');

        if ($password1 !== $password2) {
            $this->error('Password not matched!');
            return;
        }

        $user = new User;
        $user->name = $newUser;
        $user->password = Hash::make($password1);
        $user->save();

        $this->info('Admin successfully created!');
    }
}
