<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function urls()
    {
        return $this->hasMany(Url::class, 'categoryId');
    }

    public function feeds()
    {
        return $this->hasMany(Feed::class, 'categoryId');
    }
}
