## Requirements to run application

- Use Laravel [Homestead](https://laravel.com/docs/master/homestead) to run project.
- Use Sqlite3 database (already included with Laravel Homestead).

## What need configure to run the project

Most important to configure Laravel Homestead and edit .env file, because all other configuration come with project itself.

**Edit Homestead.yaml file:**

- ip - project local IP address
- provider - virtualbox (i use VirtualBox)
- folders:  
1. map: A directory in your local machine where you store project. (example: ~/Documents/RSS)  
2. to: Where your project will be stored in the virtual machine. (example: /home/vagrant/RSS)  

- sites:  
1. map: homestead.app (by default)  
2. to: A directory in your virtual machine where you store "public" project folder. (example with this project: /home/vagrant/RSS/public)  

**Edit .env file:**

Find this line DB_CONNECTION and change:
- DB_CONNECTION=sqlite  

(All other lines after DB_CONNECTION=sqlite can be deleted.)  

## PHP artisan commands and how to log in as admin

**- To create admin user:** php artisan user:create  
**- To create tables:** php artisan migrate  
**- To seed database with urls (without categories):** php artisan db:seed  
**- To update feeds:** php artisan feed:update  

**To reach admin panel and logout:**   
project local IP address/admin (example: 192.168.10.10/admin)  
project local IP address/admin/logout (example: 192.168.10.10/admin/logout)

