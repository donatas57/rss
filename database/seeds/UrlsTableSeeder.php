<?php

use Illuminate\Database\Seeder;

class UrlsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $urls = ['http://www.delfi.lt/rss/feeds/daily.xml', 'http://www.lrytas.lt/rss/', 'http://www.elektronika.lt/rss/visas/', 'http://it.lrytas.lt/rss/', 'http://auto.lrytas.lt/rss/'];

        array_map(function ($url) {
            $now = date('Y-m-d H:i:s', strtotime('now'));
            DB::table('urls')->insert([
                'url' => $url,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }, $urls);
    }
}
