<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FeedsController@showFeeds');

Route::get('/admin', ['as' => 'login', function () {
    return view('admin');
}]);

Route::group(['prefix' => 'admin'], function () {
    Route::post('/login', 'UserController@login');
    Route::get('/logout', 'UserController@logout');
    Route::post('/changePassword', 'UserController@changePassword');

    Route::resource('urls', 'UrlController', ['only' => ['store', 'update', 'destroy']]);
    Route::resource('categories', 'CategoryController', ['only' => ['store']]);
});